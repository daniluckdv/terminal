import { Component } from '@angular/core';
import { TerminalService } from 'primeng/components/terminal/terminalservice';
import { MqttService } from 'ngx-mqtt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public title = 'terminal';
  public subscription;
  public message;

  constructor(
    private terminalService: TerminalService,
    private mqttService: MqttService
  ) {
    this.terminalService.commandHandler.subscribe((command) => {
      const message = command.split(' ')[0];
      const payload = command.indexOf(' ') !== -1 ? command.substr(command.indexOf(' ') + 1) : '';
      
      this.publish(message, payload).subscribe(() => {
        this.mqttService
        .observe(`/pulse/00000000e1d88f58/response/${command}`)
        .subscribe((message) => {
          const oldTerminalRespone = document.querySelector('.ui-terminal-content > div > div');
          const newTerminalRespone = document.createElement('pre');
          newTerminalRespone.innerHTML = JSON.parse(message.payload.toString()).output;
          oldTerminalRespone.outerHTML = newTerminalRespone.outerHTML;
        });
      });
    });
  }

  private publish(command: string, payload: string) {
    return this.mqttService.publish(
      `/pulse/00000000e1d88f58/command/${command}`, payload
    );
  }
}
