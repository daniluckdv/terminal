import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TerminalModule } from 'primeng/terminal';
import { TerminalService } from 'primeng/components/terminal/terminalservice';
import { MqttModule } from 'ngx-mqtt';
import { MQTT_SERVICE_OPTIONS } from '../environments/environment';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TerminalModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS)
  ],
  providers: [ 
    TerminalService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
