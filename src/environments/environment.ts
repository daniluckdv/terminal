import { IMqttServiceOptions } from 'ngx-mqtt';

export const environment = {
  production: false
};

export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: 'q.pulses.ai',
  port: 8084,
  path: '/mqtt',
  username: 'admin',
  password: 'public',
  protocol: 'wss'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
